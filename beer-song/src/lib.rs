pub fn verse(n: i32) -> String {
    if n == 0 {
        return String::from("No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n");
    }
    if n == 1 {
        let msg = format!("{} bottle of beer on the wall, {} bottle of beer.\n{}", n, n, tail(n));
        return String::from(msg)
    } else {
        let msg = format!("{} bottles of beer on the wall, {} bottles of beer.\n{}", n, n, tail(n));
        return String::from(msg)
    }
}

pub fn tail(n: i32) -> String {
    let next = n - 1;
    if next == 0 {
        return String::from("Take it down and pass it around, no more bottles of beer on the wall.\n");
    }
    if next == 1{
        let msg = format!("Take one down and pass it around, {} bottle of beer on the wall.\n", next);
        return String::from(msg);
    } else {
        let msg = format!("Take one down and pass it around, {} bottles of beer on the wall.\n", next);
        return String::from(msg);
    }
}

pub fn sing(start: i32, end: i32) -> String {
    let mut result = String::new();
    let mut index = start;
    while index >= end {
        let verse =  verse(index);
        result.push_str(&verse);
        if index != end {
            result.push('\n');
        }
        index = index - 1;
    }
    return result;
}
