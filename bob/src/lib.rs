pub fn reply(message: &str) -> &str {


    let message_clean = message.trim().is_empty();

    if message_clean {
        return "Fine. Be that way!";
    }
    let have_letter = message.trim().chars().any(|c| c.is_alphabetic());    
    let is_a_query = message.trim_right().ends_with("?");
    if  have_letter && message.to_uppercase().eq(message) {
        if is_a_query {
            return "Calm down, I know what I'm doing!";
        }
        return "Whoa, chill out!";
    }
    if is_a_query {
        return "Sure.";
    }
    return "Whatever.";
}

