
pub fn nth(n: u32) -> Option<u32> {
    let mut prime = 2;
    if n == 0 {
        return None;
    }
    if n == 1 {
        return Option::from(prime);
    }
    let mut index :u32 = 2;
    let mut candidate :u32 = 3;
    while index <= n {
        if is_prime(candidate) {
            index = index + 1;
            prime = candidate; 
        }
        candidate = candidate + 2;
    }    
    return Option::from(prime);
}

pub fn is_prime(candidate: u32) -> bool {
    let mut divisor :u32 = 3;
    while divisor < candidate {
        if candidate % divisor == 0 {
            return false;
        }
        divisor = divisor + 2;
    }
    return true;
}